﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

BenchmarkRunner.Run<CompareAsyncVsSync>();


[MemoryDiagnoser]
public class CompareAsyncVsSync
{
    private readonly int timeDelayInMS = 100;
    private readonly int resultAfterDelay = 1;
    [Benchmark]
    public int Single_Sync()
    {
        Thread.Sleep(timeDelayInMS);
        return resultAfterDelay;
    }

    [Benchmark]
    public async Task<int> Single_Async() {
        await Task.Delay(timeDelayInMS);
        return resultAfterDelay;
    }

    [Benchmark]
    public void Multiple_Sync()
    {
        var num1 = Single_Sync();
        var num2 = Single_Sync();
        Console.WriteLine(num1 + num2);
    }

    [Benchmark]
    public async Task Multiple_Async_AwaitOneByOne()
    {
        var num1 = await Single_Async();
        var num2 = await Single_Async();
        Console.WriteLine(num1 + num2);
    }

    [Benchmark]
    public async Task Multiple_Async_AwaitMultiple()
    {
        var task1 = Single_Async();
        var task2 = Single_Async();
        var result = await Task.WhenAll(task1, task2);
        Console.WriteLine(result[0] + result[1]);
    }

}